import React, { useState, useEffect } from 'react';
import '../styles/design.css';
import moment from 'moment';
import { connect } from "react-redux";

function SalesComponent(props) {
  const [todayBillCount, setTodayBillCount] = useState(0);
  const [monthlyBillCount, setMonthlyBills] = useState(0);
  const [yearlyBillCount, setYearlyBillCount] = useState(0);

  useEffect(() => {
    getTodayBillsCount();
    getMonthlyBillsCount();
    getYearlyBillsCount();
  }, [props.billsCount]);

  function totalPrice(bills) {
    let sum = bills.reduce(function (prev, current) {
      return prev + +current.totalAmount
    }, 0);
    return sum
  }

  function getTodayBillsCount() {
    const listOfTodaysBills = props.billsList.filter(item => item.billDate === moment(new Date()).format("MMM DD, YYYY"));
    let todaysBills = totalPrice(listOfTodaysBills);
    setTodayBillCount(todaysBills);
  }

  function getMonthlyBillsCount() {
    let newDate = new Date()
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();
    const listOfMonthlyBills = props.billsList.filter(item => parseInt(new Date(item.billDate).getMonth()) + 1 === month && new Date(item.billDate).getFullYear() === year);
    let monthlyBills = totalPrice(listOfMonthlyBills);
    setMonthlyBills(monthlyBills);
  }

  function getYearlyBillsCount() {
    let newDate = new Date()
    let year = newDate.getFullYear();
    const listOfYearlyBills = props.billsList.filter(item => new Date(item.billDate).getFullYear() === year)
    let yearlyBills = totalPrice(listOfYearlyBills);
    setYearlyBillCount(yearlyBills);
  }

  return (
    <div>
      <fieldset>
        <h2>Sales</h2>
        <div className="sales" id="three">
          <p>This Year Rs. {yearlyBillCount}</p>
        </div>
        <div className="sales" id="two">
          <p>This Month Rs. {monthlyBillCount}</p>
        </div>
        <div className="sales" id="one">
          <p>Today Rs. {todayBillCount}</p>
        </div>
      </fieldset>
    </div>
  )
}

const mapStatetoProps = state => {
  return {
    billsList: state.billsList,
    billsCount: state.billsCount
  };
};

const mapDispatchToProps = dispatch => {
  return {
  };
};
export default connect(mapStatetoProps, mapDispatchToProps)(SalesComponent);