import React, { useState, useEffect } from 'react';
import '../styles/design.css';
import SalesComponent from './SalesComponent';
import { connect } from "react-redux";

function MyBillsComponent(props) {
  const [billsList, setBillsList] = useState([]);

  useEffect(() => {
    const requestOptions = {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    };
    fetch('/bills/list', requestOptions)
    .then(async response => {
      const isJson = response.headers.get('content-type')?.includes('application/json');
      const data = isJson && await response.json();
      props.GetBillsList(data);
      setBillsList(data.billsList);
    })
  }, [props.billsList]);

  return (
    <div>
      <form>
        <fieldset>
          <h2>My Bills</h2>
          <table>
            <tr>
              <th>Bill ID</th>
              <th>Bill Date</th>
              <th>Amount</th>
            </tr>
            {billsList && billsList.length > 0 && billsList.map((item, index) => 
              <tr key={index}>
                <td>{item.billID}</td>
                <td>{item.billDate}</td>
                <td>Rs. {item.totalAmount}</td>
              </tr>
            )}
            {billsList.length < 1 &&
              <tr><td colSpan="3">You Have No Bills</td></tr>
            }
          </table>
        </fieldset>
      </form>
    </div>
  )
}

const mapStatetoProps = state => {
  return {
    bills: state.billsList,
    billsCount: state.billsCount
  };
};

const mapDispatchToProps = dispatch => {
  return {
    GetBillsList: (data) => dispatch({ type: "Bills_List", data: data })
  };
};
export default connect(mapStatetoProps, mapDispatchToProps)(MyBillsComponent);