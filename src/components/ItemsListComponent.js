import React from 'react';
import { connect } from "react-redux";
import '../styles/design.css';

class ItemsListComponent extends React.PureComponent {
  constructor(props) {
    super();
    this.state = {
      itemPrice: '',
      itemName: '',
      addItemErrorMsg: '',
      itemsList: []
    };
    this.handleAddNewItem = this.handleAddNewItem.bind(this);
  }

  componentDidMount() {
    const requestOptions = {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    };
    fetch('/items/list', requestOptions)
    .then(async response => {
      const isJson = response.headers.get('content-type')?.includes('application/json');
      const data = isJson && await response.json();
      this.props.GetItemsList(data);
      this.setState({ itemsList: data && data.itemsList })
    })
  }

  handleAddNewItem() {
    if(!this.state.itemName.trim()) {
      this.setState({ addItemErrorMsg: "Item Name is Required" });
    } else if(!this.state.itemPrice) {
      this.setState({ addItemErrorMsg: "Item Price is Required" });
    } else {
      let addNewItem = {};
      addNewItem['itemName'] = this.state.itemName;
      addNewItem['itemPrice'] = this.state.itemPrice;
      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(addNewItem)
      };
      fetch('/add/item', requestOptions)
      .then(async response => {
        const isJson = response.headers.get('content-type')?.includes('application/json');
        const data = isJson && await response.json();
        this.props.UpdateItemsList(data.resObj);
        this.setState({ addItemErrorMsg: '', itemName: '', itemPrice: [...this.state.itemsList, data.resObj ] });
      })
      .catch(error => {
        console.error('There was an error!', error);
      });
    }
  }

  render() {
    let totalItemsList = this.state.itemsList || []
    return (
      <div>
        <form>
          <fieldset>
            <h2>Add Item</h2>
            <label>
              <p>Name*: </p>
              <input type="text" name="itemName" value={this.state.itemName} onChange={(e) => this.setState({ itemName: e.target.value })} />
            </label>
            <label>
              <p>Price*: </p>
              <input type="number" name="itemPrice" value={this.state.itemPrice} onChange={(e) => this.setState({ itemPrice: e.target.value })} step="1"/>
            </label>
            <p style={{color: 'red'}}>{this.state.addItemErrorMsg}</p>
            <button type="button" onClick={this.handleAddNewItem}>Submit</button>
          </fieldset>
        </form>
        <form>
          <fieldset>
            <h2>Items List</h2>
            <table>
              <tr>
                <th>Item Name</th>
                <th>Item Price</th>
              </tr>
              {totalItemsList && totalItemsList.length > 0 && totalItemsList.map((item, index) => 
                <tr key={index}>
                  <td>{item.itemName}</td>
                  <td>Rs. {item.itemPrice}</td>
                </tr>
              )}
              {totalItemsList.length < 1 &&
                <tr><td colSpan="3">No data found</td></tr>
              }
            </table>
          </fieldset>
        </form>
      </div>
    )
  }
}

const mapStatetoProps = state => {
  return {
    itemsList: state.itemsList,
    billsList: state.billsList
  };
};

const mapDispatchToProps = dispatch => {
  return {
    GetItemsList: (data) => dispatch({ type: "Items_List", data: data }),
    UpdateItemsList: (data) => dispatch({ type: "Update_Items_List", data: data })
  };
};
export default connect(mapStatetoProps, mapDispatchToProps)(ItemsListComponent);