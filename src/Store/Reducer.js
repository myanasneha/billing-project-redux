const initialState = {
  itemsList: [],
  itemsCount: 0,
  billsList: [],
  billsCount: 0
};

const reducer = (state = initialState, action) => {
  const newState = { ...state };
  switch(action.type){
    case 'Items_List':
      newState.itemsList = action.data.itemsList;
      newState.itemsCount = action.data.itemsCount;
      break;
    case 'Bills_List':
      newState.billsList = action.data.billsList;
      newState.billsCount = action.data.billsCount;
      break;
    case 'Update_Items_List':
      let newItemsList = newState.itemsList;
      newItemsList.push(action.data);
      newState.itemsList = newItemsList;
      newState.itemsCount = newItemsList.length;
      break;
    case 'Update_Bills_List':
      let updatedBillsList = newState.billsList;
      updatedBillsList.push(action.data);
      newState.billsList = updatedBillsList;
      newState.billsCount = updatedBillsList.length;
      break;
    default:
      break;
  }
  return newState;
};

export default reducer;