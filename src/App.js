// import logo from './logo.svg';
import React from 'react';
import ItemsListComponent from './components/ItemsListComponent';
import NewBillComponent from './components/NewBillComponent';
import MyBillsComponent from './components/MyBillsComponent';
import SalesComponent from './components/SalesComponent';
import './App.css';

function App() {

  return (
    <div className="wrapper">
      <ItemsListComponent />
      <NewBillComponent />
      <MyBillsComponent />
      <SalesComponent />
    </div>
  );
}

export default App;
